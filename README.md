# whou's pages

This is my static generated Codeberg Pages website.

The website is generated with [Franklin.jl](https://franklinjl.org/).

You can see it live on [whou.codeberg.page][codeberg page].

## Setup
Before running the project, make sure to download the dependencies with the Julia REPL:
```
julia
]activate .
instantiate
```

## Run
In order to serve the website locally you only need to execute the `serve.jl` script.

You may include the `--fetch` flag if you want to see API-fetched content. It is not recommended to use it because it re-fetches APIs on every reload.
```
julia serve.jl --fetch
```
A LiveServer will be listening on `http://localhost:8000/` and a browser window on the website will open.

## Deploy
First and foremost, you need to pull the `pages` submodule with
```
git submodule update --init
```

I made sure to make the deployment of any changes to the [live][codeberg page] version of the website as painless and straight-forward as possible through the simple `deploy.jl` script.
```
julia deploy.jl
```

It will produce the build, move the output to the `pages` repo, commit changes and, optionally, push them to upstream immediately.

[codeberg page]: https://whou.codeberg.page
