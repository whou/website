using Pkg
Pkg.activate(@__DIR__)
using Franklin
serve(clear=true, prerender=true)
