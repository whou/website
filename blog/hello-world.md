+++
title = "Hello, World!"
favicon = "/assets/favicons/blog.svg"
css = "/css/custom.css"
rss_description = "For quite some time I was struggling to figure out what my first blog post should be."
+++

# Hello, World!

For quite some time I was struggling to figure out what my first blog post should be. A cool article about some technology or topic I'm interested in? No, I'm not that interesting, and I don't have anything that special or new to write about. A tutorial for something programming related? Hmmmm maybe, but I also don't have any particular knowledge that isn't obvious or already out there.

So looking at some people's blog for ideas I figured out a generic "hello world" post should do. I thought it was kind of a boring idea at first, but I figured out that if I enjoyed reading other people's "hello world" post, that should be enough reason to make mine.

So, alright. Here we are...

@@big Hello, World! @@

That's it.

I'm probably gonna still struggle finding something remotely interesting to write about, but I hope a straight start should get me motivated to not be so strict about the content and finally write something, anything.

Though I still don't think I'm actually worth listening to, I'm doing this because I really want to participate and contribute on the [IndieWeb](https://indieweb.org/), not as a passive consumer anymore. All of this seems way too fun to miss out on! And besides, who doesn't like talking about hobbies and stuff you like?

Even without any of those "reasons", it wouldn't matter. Actually, it still doesn't matter. If I'm having fun doing it, why shouldn't I?

If you want to check out if I did anything cool, I'd say not yet. It's gonna take me some time to finish something solid - especially when I am juggling through various projects. But if you want, you can check me out or follow me on [Codeberg][codeberg], [GitHub][github] or [Mastodon][mastodon].

At last, here is a cool monkey photo for you to leave with:

![cute monkey holding a small mirror looking closely to his own reflection](/assets/blog/hello-world/monkepic.jpg)
\begin{center}
@@small _Photo by [Andre Mouton](https://unsplash.com/@andremouton), 2018.[^1]_ @@
\end{center}

I love monkeys.

---
[^1]: Licensed under the [Unsplash License](https://unsplash.com/license) on [Unsplash][photo].

[codeberg]: https://codeberg.org/whou
[github]: https://github.com/whouishere
[mastodon]: https://indieweb.social/@whou
[photo]: https://unsplash.com/photos/GBEHjsPQbEQ
