+++
title = "~/blog"
favicon = "/assets/favicons/blog.svg"
css = "/css/custom.css"
tags = ["index"]
+++

~~~
<style>
.absolute-right {
  position: absolute;
  top: 0;
  right: 0;
  padding-right: 12.5vw;
}

.absolute-right a {
  display: flex;
}

.absolute-right img {
  width: 1.75em;
  padding-left: unset;
}

.blog-item-title {
  line-height: 0;
  margin-top: 1.35em;
  margin-bottom: 0.5em;
}

.blog-item-date {
  font-size: 0.75em;
}
</style>
~~~

@@big Blog @@
@@absolute-right [![RSS feed icon](/assets/Feed-icon.svg)](/feed.xml) @@
---
{{ blogposts }}
