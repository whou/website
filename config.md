+++
mintoclevel = 2

ignore = [
  ".editorconfig",
  "package.json",
  "package-lock.json",
  "node_modules/",
  "pages/",
  "Project.toml",
  "Manifest.toml",
  "serve.jl",
  "deploy.jl"
]

author = "Willian Vinagre"

generate_rss = true
rss_full_content = true
rss_file = "feed"
website_title = "/home/whou"
website_descr = "whou's blog about tech, programming, and probably other stuff"
website_url   = "https://whou.codeberg.page/"
+++

<!-- global environments -->
\newenvironment{center}{
    ~~~<div style="text-align: center;">~~~
}{
    ~~~</div>~~~
}

\newenvironment{div}{
    ~~~<div style="margin: 0 auto;">~~~
}{
    ~~~</div>~~~
}
