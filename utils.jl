using Dates, TimeZones, Franklin
using HTTP, JSON
using LicenseCheck
using Printf
using CairoMakie
using Images

import Git
const git = Git.git()

const ignored_langs = [
    "CMake",
    "Makefile",
    "HTML",
    "CSS",
    "Jinja",
    "Shell"
]

function rgb8(r, g, b)::RGBf
    RGBf(r / 255, g / 255, b / 255)
end

const license_colors = Dict([
    ("GPL-3.0", rgb8(189, 0, 0)),     # red
    ("AGPL-3.0", rgb8(102, 51, 102)),  # purple
    ("LGPL-3.0", rgb8(0, 128, 51)),    # green
    ("MPL-2.0", rgb8(0, 0, 0)),       # black
    ("MIT", rgb8(125, 126, 127)), # grey
    ("Apache-2.0", rgb8(234, 121, 33)),  # orange
    ("BSD", rgb8(235, 181, 62)),  # yellow
    # add more as needed
])

# variable used to disable API fetching everytime the website reloads while serving
global should_fetch_apis::Bool = "--fetch" ∈ ARGS
if !should_fetch_apis
    Franklin.print_warning("APIs are not being fetched")
end

"""
return a file's last commit date as `DateTime`
"""
function get_last_commit_date(filepath::String)::DateTime
    out = IOBuffer()
    cmd = pipeline(`$git log -1 --pretty="format:%ci" $filepath`; stdout=out)
    run(cmd)

    return DateTime(ZonedDateTime(String(take!(out)), "y-m-d H:M:S z"))
end

function hfun_blogposts()
    local posts = readdir("blog")
    filter!(f -> endswith(f, ".md"), posts) # select all posts files
    filter!(e -> e ≠ "index.md", posts) # take index page out
    dates = [datetime2unix(get_last_commit_date(joinpath("blog", post))) for post in posts] # get each post date
    sorted = sortperm(dates; rev=true) # sort dates from most recent to oldest

    io = IOBuffer()
    for (_, post) in enumerate(sorted)
        posturi = splitext(posts[post])[1] # bad naming. not an actual URI
        postpath = joinpath("blog", posts[post])
        posturl = "/blog/$posturi/"
        title = Franklin.pagevar(postpath, :title) # find title var inside the post
        title === nothing && (title = posturi) # if not found, use file name
        date = Dates.format(get_last_commit_date(postpath), "dd/mm/YYYY - HH:MM")
        write(
            io,
            """
      <div>
          <h3 class="blog-item-title"><a href="$posturl">$title</a></h3>
          <span class="blog-item-date">$date</span>
      </div>
  """
        )
    end

    return String(take!(io))
end

function hfun_getfilehist()
    # file commit history URL from the git repository
    commithist_url = "https://codeberg.org/whou/website/commits/branch/master/"
    sourcefile = Franklin.get_source()

    commithist_url * sourcefile
end

"""
fetch repository topics from its respective API
"""
function get_topics(url::String)::Vector{String}
    println("fetching $url topics...")
    # get `OWNER/REPO` as String
    id = match(r"[^\/]+\/[^\/]+$", url).match

    if occursin("codeberg.org", url)
        global res = HTTP.get("https://codeberg.org/api/v1/repos/$id/topics")
        global field_name = "topics"
    elseif occursin("github.com", url)
        global res = HTTP.get("https://api.github.com/repos/$id/topics")
        global field_name = "names"
    elseif occursin("gitlab.com", url)
        owner, project = split(id, "/")
        global res = HTTP.get("https://gitlab.com/api/v4/projects/$owner%2F$project")
        global field_name = "topics"
    else
        error("'$url' API not supported")
    end

    data = JSON.parse(String(res.body))
    get(data, field_name, [])
end

function hfun_fetchrepotopics(urls::Vector{String})
    if !should_fetch_apis
        return ""
    end

    topics = String[]
    foreach(url -> append!(topics, get_topics(url)), urls)

    # don't even bother returning HTML if no topics were found
    if isempty(topics)
        return ""
    end

    io = IOBuffer()
    write(io, """<div class="topics">\n""")

    foreach(tag -> write(io, """<span class="topic-tag">$tag</span>\n"""), topics)

    write(io, """</div>""")

    return String(take!(io))
end

function get_main_languages(url::String)::Vector{String}
    println("fetching $url languages...")
    # get `OWNER/REPO` as String
    id = match(r"[^\/]+\/[^\/]+$", url).match

    if occursin("codeberg.org", url)
        global res = HTTP.get("https://codeberg.org/api/v1/repos/$id/languages")
    elseif occursin("github.com", url)
        global res = HTTP.get("https://api.github.com/repos/$id/languages")
    elseif occursin("gitlab.com", url)
        owner, project = split(id, "/")
        global res = HTTP.get("https://gitlab.com/api/v4/projects/$owner%2F$project/languages")
    else
        error("'$url' API not supported")
    end

    data = JSON.parse(String(res.body))

    biglangs::Vector{String} = []

    total_loc = 0
    biggest::Tuple{String,Number} = ("", 0)
    for (lang, value) in pairs(data)
        (_, loc) = biggest
        if value > loc
            biggest = (lang, value)
        end

        # only gitlab divides loc by percentage
        if occursin("gitlab.com", url)
            if value >= 15 && lang != biggest
                push!(biglangs, lang)
            end
        else
            total_loc += value
        end
    end
    push!(biglangs, biggest[1])

    if !occursin("gitlab", url)
        # push any language that has more than 15% of the total LOC
        for (lang, value) in pairs(data)
            if lang != biggest[1] && lang ∉ ignored_langs
                loc_percent = (100 * value) / total_loc
                if loc_percent >= 15
                    push!(biglangs, lang)
                end
            end
        end
    end

    return biglangs
end

function get_projects_urls()::Vector{String}
    data = JSON.parsefile("projects.json")
    projects = get(data, "projects", [])
    if isempty(projects)
        error("projects JSON data is empty")
    end
    return projects
end

language_data = Dict{String,Int}()

function populate_language_data()
    global language_data
    if !should_fetch_apis || !isempty(language_data)
        return
    end

    projects::Vector{String} = get_projects_urls()

    for project in projects
        for lang in get_main_languages(project)
            frequency = get(language_data, lang, 0)
            language_data[lang] = frequency + 1
        end
    end
end

function hfun_languagechart2string()
    populate_language_data()
    global language_data

    projects_total = 0
    for num::Int in values(language_data)
        projects_total += num
    end

    str = ""
    for (lang::String, val::Int) in language_data
        # calculate the percentage that `val` represents in `projects_total`
        percentage::Float64 = (100 * val) / projects_total

        str = str * @sprintf("%s is %.1f%%, ", lang, percentage)
    end
    # remove leading comma
    str = chop(str, tail=2)

    return str
end

function hfun_generatelanguagechart()
    populate_language_data()
    global language_data
    if !should_fetch_apis
        return ""
    end

    data = collect(values(language_data))
    labels = collect(keys(language_data))

    res = HTTP.get("https://raw.githubusercontent.com/ozh/github-colors/master/colors.json")
    colordata = JSON.parse(String(res.body))
    colors = map(lang -> Makie.Colors.parse(Makie.Colors.Colorant, colordata[lang]["color"]), labels)

    fig, axis, plot = pie(data,
        color=colors,
        transparency=true,
        strokecolor=:white,
        strokewidth=3,
        axis=(autolimitaspect=1,)
    )
    hidedecorations!(axis)
    hidespines!(axis)

    # draw legends
    # some black magic from https://discourse.julialang.org/t/labels-in-pie-charts-in-makie/94088/7
    theta = (cumsum(data) - data / 2) .* 360 / sum(data)
    sincos = sincosd.(theta)
    for (label, theta_idx, (x, y)) in zip(labels, theta, sincos)
        radius = 90 < theta_idx < 270 ? 1.25 : 1.10
        text!(axis, label,
            position=radius .* (y, x),
            color=RGBf(0.9, 0.9, 0.9),
            fontsize=18,
            align=(:center, :center)
        )
    end

    imgpath = Franklin.parse_rpath("/_assets/language_chart.png", canonical=true)
    save(imgpath, fig)

    img = load(imgpath)
    colorchannel = RGBA.(img)
    transimg = map(c -> c.r == 1.0 && c.g == 1.0 && c.b == 1.0 ? RGBA(c.r, c.g, c.b, 0) : c, colorchannel)
    save(imgpath, transimg)

    return Franklin.parse_rpath("/_assets/language_chart.png")
end

function get_licenses(url::String)::Array{String}
    println("fetching $url license...")
    # get `OWNER/REPO` as String
    id = match(r"[^\/]+\/[^\/]+$", url).match

    # get repository default branch
    if occursin("codeberg.org", url)
        global branchres = HTTP.get("https://codeberg.org/api/v1/repos/$id")
    elseif occursin("github.com", url)
        global branchres = HTTP.get("https://api.github.com/repos/$id")
    elseif occursin("gitlab.com", url)
        owner, project = split(id, "/")
        global branchres = HTTP.get("https://gitlab.com/api/v4/projects/$owner%2F$project")
    else
        error("'$url' API not supported")
    end

    json = JSON.parse(String(branchres.body))
    branch = get(json, "default_branch", "master")

    function fetch_repo_license(filename::String)::HTTP.Response
        res = HTTP.Response(404)
        if occursin("codeberg.org", url)
            res = HTTP.get("https://codeberg.org/$id/raw/branch/$branch/$filename")
        elseif occursin("github.com", url)
            res = HTTP.get("https://raw.githubusercontent.com/$id/$branch/$filename")
        elseif occursin("gitlab.com", url)
            res = HTTP.get("https://gitlab.com/$id/-/raw/$branch/$filename")
        else
            error("'$url' API not supported")
        end
        res
    end

    # this code terrifies me
    # i hate exceptions dude
    res = try
        fetch_repo_license("COPYING")
    catch e
        if isa(e, HTTP.StatusError) && e.status == 404
            try
                fetch_repo_license("LICENSE")
            catch e
                if isa(e, HTTP.StatusError) && e.status == 404
                    try
                        fetch_repo_license("LICENCE")
                    catch e
                        if isa(e, HTTP.StatusError) && e.status == 404
                            error("can't find '$url' license")
                        else
                            throw(e)
                        end
                    end
                else
                    throw(e)
                end
            end
        else
            throw(e)
        end
    end

    license_text = String(res.body)
    result = licensecheck(license_text)

    return result.licenses_found
end

license_data = Dict{String,Int}()

function populate_license_data()
    global license_data
    if !should_fetch_apis || !isempty(license_data)
        return
    end

    projects::Vector{String} = get_projects_urls()

    for project in projects
        for license in get_licenses(project)
            frequency = get(license_data, license, 0)
            license_data[license] = frequency + 1
        end
    end
end


function hfun_licensechart2string()
    populate_license_data()
    global license_data

    licenses_total = 0
    for num::Int in values(license_data)
        licenses_total += num
    end

    str = ""
    for (license::String, val::Int) in license_data
        percentage::Float64 = (100 * val) / licenses_total

        str = str * @sprintf("%s is %.1f%%, ", license, percentage)
    end
    str = chop(str, tail=2)

    return str
end

function hfun_generatelicensechart()
    populate_license_data()
    global license_data
    if !should_fetch_apis
        return ""
    end

    data = collect(values(license_data))
    labels = collect(keys(license_data))

    colors = map(license -> license_colors[license], labels)

    fig, axis, plot = pie(data,
        color=colors,
        transparency=true,
        strokecolor=:white,
        strokewidth=3,
        axis=(autolimitaspect=1,)
    )
    hidedecorations!(axis)
    hidespines!(axis)

    theta = (cumsum(data) - data / 2) .* 360 / sum(data)
    sincos = sincosd.(theta)
    for (label, theta_idx, (x, y)) in zip(labels, theta, sincos)
        radius = 90 < theta_idx < 270 ? 1.25 : 1.10
        text!(axis, label,
            position=radius .* (y, x),
            color=RGBf(0.9, 0.9, 0.9),
            fontsize=18,
            align=(:center, :center)
        )
    end

    imgpath = Franklin.parse_rpath("/_assets/license_chart.png", canonical=true)
    save(imgpath, fig)

    img = load(imgpath)
    colorchannel = RGBA.(img)
    transimg = map(c -> c.r == 1.0 && c.g == 1.0 && c.b == 1.0 ? RGBA(c.r, c.g, c.b, 0) : c, colorchannel)
    save(imgpath, transimg)

    return Franklin.parse_rpath("/_assets/license_chart.png")
end
