# script used to build and deploy a new static build of the website to git

const usage = """USAGE: deploy.jl [OPTIONS]
\t-m "[message]"      Commit with the given message
\t-y, --push          Push commits to upstream
\t-h, --help          Print this help text"""

push_confirm = false
commit_msg = ""

# parse arguments
begin
    local prev_arg = ""
    for arg in ARGS
        if arg == "-h" || arg == "--help"
            println(usage)
            exit(0)
        elseif arg == "-y" || arg == "--push"
            global push_confirm
            push_confirm = true
        elseif prev_arg == "-m"
            global commit_msg
            commit_msg = arg
        end

        prev_arg = arg
    end
end

# APIs should always be fetched on deploy. see `/utils.jl`
push!(ARGS, "--fetch")

const dir = @__DIR__

# activating the project
using Pkg
Pkg.activate(dir)
Pkg.instantiate()

import Franklin
import NodeJS
import Git
const npm = NodeJS.npm_cmd()
const git = Git.git()

"""
checks if a npm package is installed locally
"""
function is_pkg_installed(pkg::String)
    try
        read(`$npm list $pkg`, String)
        return true
    catch _
        return false
    end
end

function build_and_deploy(commit::String)
    # if highlight.js is not installed, install it with npm
    println("\nChecking for dependencies...")
    is_pkg_installed("highlight.js") || run(`$npm install highlight.js`)

    # optimize and minify static output with Franklin
    println("\nOptimizing the website...")
    Franklin.optimize()

    # julia's `cp()` can't directly copy `__site/` recursively without nuking the `pages/` submodule
    println("\nCopying static output to codeberg pages repo...")
    for file in readdir(joinpath(dir, "__site"))
        cp(joinpath(dir, "__site", file), joinpath(dir, "pages", file); force=true)
    end

    # commit to pages repo
    println("\nCommiting website output...")
    cd("$dir/pages")
    run(`$git add --all`)
    run(`$git commit -m "$commit"`)

    cd(dir)
    run(`$git add --all`)
    run(`$git commit -m "$commit"`)
end

if commit_msg == ""
    print("\nEnter your commit message for deployment: ")
    commit_msg = readline()
end

const deploy_perf = @elapsed @eval build_and_deploy(commit_msg)
println("\nDeployment took ", deploy_perf, " seconds.")

function push_commits()
    cd("$dir/pages")
    run(`$git push origin HEAD:main`)

    cd(dir)
    run(`$git push`)
end

if !push_confirm
    print("\nDo you want to push your changes to upstream? (y/n) ")
    const choice = readline()
    if lowercase(choice) == "y" || lowercase(choice) == "yes"
        push_commits()
    end
else
    push_commits()
end
