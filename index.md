+++
title = "~/"
favicon = "/assets/favicon.svg"
css = "/css/custom.css"
tags = ["index"]
+++

~~~
<style>
.circle-crop {
  display: flex;
  justify-content: center;
}

.circle-crop img {
  width: auto;
  padding-left: 0;
  border-radius: 50%;
}
</style>
~~~

\begin{center}
@@big Willian Vinagre @@
@@small a.k.a. whou @@
\end{center}

@@circle-crop ![](/assets/avatar.jpg) @@
\\
\begin{center}
computer nerd, wannabe programmer
\end{center}
\\

My main focus is game programming, especially on the graphics engines side.

Big advocate for the Free Software movement.

## socials
codeberg - [whou][codeberg]

github - [whouishere][github]

email - [willianvinagre11@gmail.com][email]

matrix - [@whou:matrix.org][matrix]

mastodon - ~~~<a rel="me" href="https://indieweb.social/@whou">@whou@indieweb.social</a>~~~

twitter - [@whouishere][twitter]

[codeberg]: https://codeberg.org/whou
[github]: https://github.com/whouishere
[email]: mailto:willianvinagre11@gmail.com
[matrix]: https://matrix.to/#/@whou:matrix.org
[twitter]: https://twitter.com/twitter
