+++
title = "~/projects"
favicon = "/assets/favicons/projects.svg"
+++

~~~
<style>
.card,
.card-left,
.card-right {
  border: 1px solid;
  border-radius: 10px;
  margin: 1rem auto;
}

.card .header-anchor,
.card-left .header-anchor,
.card-right .header-anchor {
  display: none;
}

.card-left {
  border-color: grey;
  margin-right: 25%;
}

.card-right {
  border-color: lightgrey;
  margin-left: 25%;
}

.card *,
.card-left *,
.card-right * {
  margin: 1rem;
}

.card-left * {
  text-align: left;
}

.card-right * {
  text-align: right;
}

.topics {
  display: block;
  margin: .75rem;
  margin-top: -.25rem;
}

.topic-tag {
  display: inline-block;
  margin: 0;
  margin-top: .25rem;
  padding: 1px 12px;
  font-size: small;
  border-radius: 2em;
  background: #2D2D2D;
}

code {
  margin: 0 !important;
}

summary {
  cursor: pointer;
}
</style>
~~~

<!-- \card{side (left or right)}{title}{description}{url} -->
\newcommand{\card}[4]{
    ~~~<div class="card-!#1">~~~
### [#2](#4)
#3
    {{ fetchrepotopics #4 }}
    ~~~</div>~~~
}

~~~
<details>
<summary>Chart of used programming languages by projects</summary>

<img src="{{ generatelanguagechart }}"
     alt="Pie chart with each piece representing a different programming language and the amount of projects I use that language in. {{ languagechart2string }}."/>

</details>
~~~

~~~
<details>
<summary>Chart of the usage of different licenses across projects</summary>

<img src="{{ generatelicensechart }}"
     alt="Pie chart with each piece representing the frequency of usage of a license across my projects. {{ licensechart2string }}."/>

</details>
~~~

\card{left}{simpleutils}{
    Small Unix coreutils package written in Go, meant to be as small, clean and simple as possible.
}{https://codeberg.org/whou/simpleutils}

\card{right}{CDN Pilled}{
    Fast asynchronous CDN server in bleeding edge C++.
}{https://codeberg.org/whou/cdnpilled}

\card{left}{luafb}{
    Window pixel framebuffer framework scriptable with Lua, implemented in Rust.
}{https://codeberg.org/whou/luafb}

\card{right}{Cubes n' Stuff}{
    3D Minecraft clone made in a custom modern C++ OpenGL engine.
}{https://codeberg.org/whou/cubesnstuff}

\card{left}{Spacing}{
    Chat server experiment, with a Dart server and a Kotlin client.
}{https://codeberg.org/whou/spacing}

\card{right}{MyBad}{
    Minimal C99 library for modern error handling in C.
}{https://codeberg.org/whou/mybad}

\card{left}{wyrmstatus}{
    BookWyrm API client.
}{https://codeberg.org/whou/wyrmstatus}

\card{right}{raysnake}{
    Almost complete snake clone in plain C99.
}{https://codeberg.org/whou/raysnake}

\card{left}{GloomyTOP}{
    Any artist's saddest songs ranked.
}{https://codeberg.org/whou/gloomytop}

\card{right}{create-vanilla}{
    A CLI tool to setup a vanilla JavaScript project.
}{https://github.com/whouishere/create-vanilla}

\card{left}{Noticer}{
    Small GTK application to notify yourself at certain times of the day.
}{https://codeberg.org/whou/noticer}

\card{right}{Imperialist Collector}{
    Mobile "catch-the-ball" game made on libGDX.
}{https://github.com/whouishere/imperialist-collector}

\card{left}{txtshare}{
    Stupid server to share text between devices.
}{https://codeberg.org/whou/txtshare}

\card{right}{Woolly}{
    Simple CLI Mastodon client written in C#.
}{https://codeberg.org/whou/Woolly}

\card{left}{Doorpad}{
    Lightweight and simplified Microsoft Notepad alternative.
}{https://codeberg.org/whou/doorpad}

\card{right}{Better CMD}{
    ~~~<s>Enhanced</s>~~~ Windows `CMD.EXE` implementation in Rust.
}{https://codeberg.org/whou/bcmd}
